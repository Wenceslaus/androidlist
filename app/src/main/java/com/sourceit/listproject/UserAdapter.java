package com.sourceit.listproject;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import androidx.annotation.NonNull;
import androidx.databinding.DataBindingUtil;
import androidx.recyclerview.widget.RecyclerView;

import com.sourceit.listproject.databinding.ItemUserBinding;

import java.util.List;

public class UserAdapter extends RecyclerView.Adapter<UserAdapter.UserViewHolder> {

    private Context context;
    private List<UserModel> users;
    private OnItemClickListener listener;

    public UserAdapter(Context context, List<UserModel> users, OnItemClickListener listener) {
        this.context = context;
        this.users = users;
        this.listener = listener;
    }

    public void addUser(UserModel name) {
        users.add(name);
//        notifyDataSetChanged();
        notifyItemInserted(users.size() - 1);
    }

    @NonNull
    @Override
    public UserViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        ItemUserBinding b = DataBindingUtil.inflate(LayoutInflater.from(context),
                R.layout.item_user, parent, false);
        return new UserViewHolder(b, listener);
    }

    @Override
    public void onBindViewHolder(@NonNull UserViewHolder holder, int position) {
        UserModel user = users.get(position);
//        holder.name.setText(userName);
        holder.bind(user);
    }

    @Override
    public int getItemCount() {
        return users.size();
    }

    static class UserViewHolder extends RecyclerView.ViewHolder {

        OnItemClickListener listener;
        ItemUserBinding binding;

        public UserViewHolder(ItemUserBinding b, OnItemClickListener listener) {
            super(b.getRoot());
            this.binding = b;
            this.listener = listener;
        }

        public void bind(final UserModel user) {
            binding.setUser(user);
            binding.root.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    listener.onItemClick(user);
                }
            });
        }
    }

}
