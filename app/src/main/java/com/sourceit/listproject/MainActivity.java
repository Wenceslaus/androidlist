package com.sourceit.listproject;

import androidx.appcompat.app.AppCompatActivity;

import android.os.Bundle;

public class MainActivity extends AppCompatActivity implements Navigator {

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
        getSupportFragmentManager()
                .beginTransaction()
                .replace(R.id.container, ListFragment.newInstance())
                .commit();
    }

    @Override
    public void showDetails(UserModel userModel) {
        getSupportFragmentManager()
                .beginTransaction()
                .replace(R.id.container, UserFragment.newInstance(userModel))
                .addToBackStack(null)
                .commit();
    }
}
