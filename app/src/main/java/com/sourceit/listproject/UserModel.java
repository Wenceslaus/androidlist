package com.sourceit.listproject;

import android.os.Parcel;
import android.os.Parcelable;

public class UserModel implements Parcelable {
    public String name;
    public int age;

    public UserModel(String name, int age) {
        this.name = name;
        this.age = age;
    }

    protected UserModel(Parcel in) {
        name = in.readString();
        age = in.readInt();
    }

    public static final Creator<UserModel> CREATOR = new Creator<UserModel>() {
        @Override
        public UserModel createFromParcel(Parcel in) {
            return new UserModel(in);
        }

        @Override
        public UserModel[] newArray(int size) {
            return new UserModel[size];
        }
    };

    @Override
    public int describeContents() {
        return 0;
    }

    @Override
    public void writeToParcel(Parcel dest, int flags) {
        dest.writeString(name);
        dest.writeInt(age);
    }
}
