package com.sourceit.listproject;

import android.content.Context;
import android.os.Bundle;

import androidx.annotation.NonNull;
import androidx.databinding.DataBindingUtil;
import androidx.fragment.app.Fragment;
import androidx.recyclerview.widget.LinearLayoutManager;

import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import com.sourceit.listproject.databinding.FragmentListBinding;

import java.util.List;

public class ListFragment extends Fragment implements OnItemClickListener {

    public static ListFragment newInstance() {
        return new ListFragment();
    }

    private FragmentListBinding binding;
    private UserAdapter userAdapter;
    private Navigator navigator;
    private List<UserModel> users = Generator.generate();

    @Override
    public void onAttach(@NonNull Context context) {
        super.onAttach(context);
        navigator = (Navigator) context;
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle b) {
        binding = DataBindingUtil.inflate(inflater, R.layout.fragment_list, container, false);
        binding.add.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                userAdapter.addUser(new UserModel("New user", 40));
            }
        });
        binding.myList.setLayoutManager(new LinearLayoutManager(binding.getRoot().getContext()));
        userAdapter = new UserAdapter(binding.getRoot().getContext(), users, this);
        binding.myList.setAdapter(userAdapter);
        return binding.getRoot();
    }

    @Override
    public void onItemClick(UserModel user) {
        navigator.showDetails(user);
    }
}
