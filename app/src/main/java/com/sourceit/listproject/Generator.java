package com.sourceit.listproject;

import java.util.ArrayList;
import java.util.List;

public class Generator {
    public static List<UserModel> generate() {
        ArrayList<UserModel> list = new ArrayList<>();
        list.add(new UserModel("Vasya Ivanov", 29));
        list.add(new UserModel("Petya Petrov", 25));
        list.add(new UserModel("Ivan Sidorov", 20));
        list.add(new UserModel("Vasya Ivanov", 29));
        list.add(new UserModel("Petya Petrov", 25));
        list.add(new UserModel("Ivan Sidorov", 20));
        list.add(new UserModel("Vasya Ivanov", 29));
        list.add(new UserModel("Petya Petrov", 25));
        list.add(new UserModel("Ivan Sidorov", 20));
        list.add(new UserModel("Vasya Ivanov", 29));
        list.add(new UserModel("Petya Petrov", 25));
        list.add(new UserModel("Ivan Sidorov", 20));
        list.add(new UserModel("Vasya Ivanov", 29));
        list.add(new UserModel("Petya Petrov", 25));
        list.add(new UserModel("Ivan Sidorov", 20));
        list.add(new UserModel("Vasya Ivanov", 29));
        list.add(new UserModel("Petya Petrov", 25));
        list.add(new UserModel("Ivan Sidorov", 20));
        list.add(new UserModel("Vasya Ivanov", 29));
        list.add(new UserModel("Petya Petrov", 25));
        list.add(new UserModel("Ivan Sidorov", 20));
        return list;
    }
}
