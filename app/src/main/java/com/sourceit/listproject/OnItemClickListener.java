package com.sourceit.listproject;

public interface OnItemClickListener {
    void onItemClick(UserModel userName);
}
