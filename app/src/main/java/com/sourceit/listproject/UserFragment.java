package com.sourceit.listproject;

import android.os.Bundle;

import androidx.databinding.DataBindingUtil;
import androidx.fragment.app.Fragment;

import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import com.sourceit.listproject.databinding.FragmentUserBinding;

public class UserFragment extends Fragment {

    private static final String ARG_NAME = "name";
    private static final String ARG_AGE = "age";
    private static final String ARGUMENTS = "args";

    private String name;
    private int age;

    private UserModel user;

    public static UserFragment newInstance(UserModel user) {
        UserFragment fragment = new UserFragment();
        Bundle args = new Bundle();
//        args.putString(ARG_NAME, user.name);
//        args.putInt(ARG_AGE, user.age);
        args.putParcelable(ARGUMENTS, user);
        fragment.setArguments(args);
        return fragment;
    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        if (getArguments() != null) {
//            name = getArguments().getString(ARG_NAME);
//            age = getArguments().getInt(ARG_AGE);
            user = (UserModel) getArguments().getParcelable(ARGUMENTS);
        }
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        FragmentUserBinding binding = DataBindingUtil.inflate(inflater, R.layout.fragment_user, container, false);
        binding.name.setText(user.name);
        binding.age.setText(String.valueOf(user.age));
        return binding.getRoot();
    }
}
