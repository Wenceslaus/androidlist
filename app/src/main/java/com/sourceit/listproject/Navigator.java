package com.sourceit.listproject;

public interface Navigator {
    void showDetails(UserModel userModel);
}
